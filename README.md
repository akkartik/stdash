# Introduction

Structured Data Shell (`stdash`) is an attempt to re-invent the Unix shell and core utilities to add typed inputs and outputs, standardize configuration conventions and separate the formatting of output from the data being displayed.

The Unix Philosophy has served the sofware community well for over a half century.
The Unix shell (`sh`, `bash`, `zsh` etc) and the coreutils (`ls`, `sort`, `uniq` etc) when combined with pipes provide a versatile and powerful toolkit to solve all sorts of problems.

## Unix Philosophy

1. Make each program do one thing well.
1. Expect the output of each program to become the input of another.
1. Write programs to handle plain (ASCII) text streams as a universal interface.

`stdash` attempts to update the unwritten rule above to match changes in software development philosophy in the half-century since the birth of Unix.

### Shortcomings

Rule 1 holds up and remains the best way to build composable tools.
If anything, the definition of *one thing* can become even more granular.

The second rule is vaguely stated and not followed as much in practice.
The standard way to pass the output of one program to another is to use a pipe `|`.
The output of program on the left of the pipe becomes the input of the program on the right.

- Parsing the output of a given program is difficult given that tokens are not delimited in a consistent way.
- There is no way to use the output of a single command as input to multiple commands.
- Named pipes require creating files on the filesystem.
- Many programs also change their output when in interactive mode, to introduce ANSI escape codes for formatting.
  This can cause the output to become less usable for other programs.

There are two shortcomings of the third rule above that have emerged over time:

- ASCII as the encoding for text is too restrictive today. UTF-8 is almost universally accepted, and should be the new standard for text.
- Text as the universal interface is too unstructured and having to parse the text into meaningful tokes adds overhead to each program.

## New Proposal

### Updated Unix Philosophy

1. Make each program do one thing well.
1. Have programs emit structured data, so other programs can easily consume it.
   Make the shell responsible for formatting/layout during interactive use. No more escape sequences and TTY specific code.
1. Use UTF-8 instead of ASCII for text.
1. Allow for output of one program to be passed to multiple programs.
   This means that pipes are not strictly FIFO anymore.
   For example, to take a set of files in one folder and create a corresponding file with a different extension and the same permissions:

       list files ─┬→ filenames ──→ substitution ─┬→ create new files
                   └────────→ permissions ────────┘

# Design

# Implementation Details

### Conventions

- Each program has the following directory structure:

      bin/
      bin/<program-name>
      config/
        config files (read in lexical order)
      templates/
        template files that the output uses for interactive use.
      types/
      types/input.types
      types/output.types

- Each program's execution depends on three things: input, configuration and output templates.
- Configuration is read from the following locations (in order): command line, environment variables, user configuration file, system wide configuration, application defaults.
- Output templates are also read from user configuration, system wide configuration and application defaults.
- The specific template to use is defined in the config.

## Example Syntax

    ls |a
    a|.files (map |.filename sub '["\.mp4$", "srt"]') |b
    zip <(b|) <(a|.files.permissions) | map | touch

- `|` remains the pipe symbol.
- anything directly following the `|` names the pipe - valid names are alphanumeric.
- a line ending with a named pipe continues on the next line.
- using the output of a named pipe involves putting the name before the `|` symbol (e.g. `a|`).
- a `.` selects a subset of the data from a pipe.
- `map` runs each member of a collection through the command that follows.
- zip combines each member of collections from the given pipes into one collection:

      [
        {
          "filename": ...
          "permissions": ...
        },
        ...
      ]

## Type Checking Command Pipelines
