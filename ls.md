#LS

## Name

ls - list directory contents

## Synopsis

ls [OPTION].. [PATH]...

## Description

List information about `PATH`s (the current directory by default).

### Filtering Options

### Data Options

###
